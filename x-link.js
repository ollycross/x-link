(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define([], factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.XLink = factory();
    }
}(this, function() {
    "use strict";

    var XLink = function(options, container) {
        var self = this;
        options = options || {};
        container = container || document;

        for (var option in options) {
            if (options.hasOwnProperty(option)) {
                self.options[option] = options[option];
            }
        }


        if (!container) {
            console.error("No valid container supplied. Aborting.");
            return;
        }

        self.init(container);
    };

    XLink.prototype.options = {
        onclick: function() {},
        exclude: ""
    };

    XLink.prototype.init = function(container) {
        var self = this;

        var _mapPatterns = function(pattern) {
            return new RegExp("(^| )" + pattern + "($| )");
        };

        var _handleLoaded = function() {
            var a = document.getElementsByTagName("a");
            for (var i = a.length - 1; i >= 0; i--) {
                var el = a[i];

                if (!el.href) {
                    continue;
                }

                if(!el.href.match(/^https?:\/\//)) {
                    continue;
                }

                if (self.options.exclude) {
                    var exclude = false;
                    var patterns = self.options.exclude.split(" ").map(_mapPatterns);
                    for (var p = patterns.length - 1; p >= 0; p--) {
                        if (patterns[p].test(el.className)) {
                            exclude = true;
                            break;
                        }
                    }
                    if (exclude) {
                        continue;
                    }
                }
                if (window.location.host !== el.host) {
                    if (!el.target) {
                        el.target = "_blank";
                    }

                    if (typeof self.options.onclick === "function") {
                        el.onclick = self.options.onclick;
                    }
                }
            }
        };

        if (/comp|inter|loaded/.test(document.readyState)) {
            _handleLoaded();
        } else {
            document.addEventListener("DOMContentLoaded", _handleLoaded);
        }
    };

    return XLink;
}));