X-Link will grab any links it finds within a given container.

If the `href` attribute points to a host that is not the same as the current host, it's `target` attribute will be set to `_blank` i.e. new window / tab (depending on browser behaviour).

Elements which already have a `target` attribute will be ignored.

### Installation

```
npm install x-link
```

### Usage
```
var XLink = require("x-link");

// Grab all links on page
XLink.init();

// Grab only links on within "#my-links"
var el = document.getElementById("my-links");
XLink.init(el);
```